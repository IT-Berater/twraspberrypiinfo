package de.wenzlaff.twraspberrypiinfo;

import static io.restassured.RestAssured.given;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class InfoResourceTest {

	@Test
	public void testHelloEndpoint() {
		given().when().get("/info").then().statusCode(200);
	}

}