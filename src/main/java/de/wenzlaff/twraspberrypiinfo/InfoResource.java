package de.wenzlaff.twraspberrypiinfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST - Service.
 * 
 * @author Thomas Wenzlaff
 *
 */
@Path("/info")
public class InfoResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String systeminfos() {
		return PiInfo.getSystemInfos();
	}
}