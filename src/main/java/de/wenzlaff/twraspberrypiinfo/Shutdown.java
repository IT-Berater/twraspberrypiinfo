package de.wenzlaff.twraspberrypiinfo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;

/**
 * Shutdown des Pi.
 * 
 * @author Thomas Wenzlaff
 *
 */
@Path("/shutdown")
public class Shutdown {

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String jetzt() {

		String ergebnis;
		try {
			ergebnis = "OK";
			GpioController gpio = GpioFactory.getInstance();
			gpio.shutdown();
		} catch (Exception e) {
			ergebnis = "Fehler beim Shutdown: " + e.getLocalizedMessage();
		}

		return ergebnis;
	}

}
