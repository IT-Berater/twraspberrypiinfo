package de.wenzlaff.twraspberrypiinfo;

import java.io.IOException;
import java.text.ParseException;

import org.json.simple.JSONObject;

import com.pi4j.platform.PlatformManager;
import com.pi4j.system.NetworkInfo;
import com.pi4j.system.SystemInfo;

/**
 * System Infos.
 * 
 * @author Thomas Wenzlaff
 *
 */
public class PiInfo {

	@SuppressWarnings("unchecked")
	public static String getSystemInfos() {

		JSONObject root = new JSONObject();

		try {
			try {
				root.put("Platform Name", PlatformManager.getPlatform().getLabel());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Platform ID", PlatformManager.getPlatform().getId());
			} catch (UnsupportedOperationException ex) {
			}

			try {
				root.put("Serial Number", SystemInfo.getSerial());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CPU Revision", SystemInfo.getCpuRevision());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CPU Architecture", SystemInfo.getCpuArchitecture());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CPU Part", SystemInfo.getCpuPart());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CPU Temperature", SystemInfo.getCpuTemperature());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CPU Core Voltage", SystemInfo.getCpuVoltage());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CPU Model Name", SystemInfo.getModelName());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Processor", SystemInfo.getProcessor());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Hardware", SystemInfo.getHardware());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Hardware Revision", SystemInfo.getRevision());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Is Hard Float ABI", SystemInfo.isHardFloatAbi());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Board Type", SystemInfo.getBoardType().name());
			} catch (UnsupportedOperationException ex) {
			}

			try {
				root.put("Total Memory", SystemInfo.getMemoryTotal());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Used Memory", SystemInfo.getMemoryUsed());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Free Memory", SystemInfo.getMemoryFree());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Shared Memory", SystemInfo.getMemoryShared());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Memory Buffers ", SystemInfo.getMemoryBuffers());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Cached Memory", SystemInfo.getMemoryCached());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("SDRAM_C Voltage", SystemInfo.getMemoryVoltageSDRam_C());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("SDRAM_I Voltage", SystemInfo.getMemoryVoltageSDRam_I());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("SDRAM_P Voltage", SystemInfo.getMemoryVoltageSDRam_P());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("OS Name", SystemInfo.getOsName());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("OS Version", SystemInfo.getOsVersion());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("OS Architecture", SystemInfo.getOsArch());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("OS Firmware Build", SystemInfo.getOsFirmwareBuild());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("OS Firmware Date", SystemInfo.getOsFirmwareDate());
			} catch (UnsupportedOperationException ex) {
			}

			root.put("Java Vendor", SystemInfo.getJavaVendor());
			root.put("Java Vendor URL", SystemInfo.getJavaVendorUrl());
			root.put("Java Version ", SystemInfo.getJavaVersion());
			root.put("Java VM", SystemInfo.getJavaVirtualMachine());
			root.put("Java Runtime", SystemInfo.getJavaRuntime());
			root.put("Hostname", NetworkInfo.getHostname());

			for (String ipAddress : NetworkInfo.getIPAddresses())
				root.put("IP Addresses", ipAddress);
			for (String fqdn : NetworkInfo.getFQDNs())
				root.put("FQDN", fqdn);
			for (String nameserver : NetworkInfo.getNameservers())
				root.put("Nameserver", nameserver);

			try {
				root.put("H264 Codec Enabled", SystemInfo.getCodecH264Enabled());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("MPG2 Codec Enabled", SystemInfo.getCodecMPG2Enabled());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("WVC1 Codec Enabled", SystemInfo.getCodecWVC1Enabled());
			} catch (UnsupportedOperationException ex) {
			}

			try {
				root.put("ARM Frequency", SystemInfo.getClockFrequencyArm());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("CORE Frequency ", SystemInfo.getClockFrequencyCore());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("H264 Frequency", SystemInfo.getClockFrequencyH264());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("ISP Frequency", SystemInfo.getClockFrequencyISP());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("V3D Frequency", SystemInfo.getClockFrequencyV3D());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("UART Frequency ", SystemInfo.getClockFrequencyUART());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("PWM Frequency", SystemInfo.getClockFrequencyPWM());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("EMMC Frequency ", SystemInfo.getClockFrequencyEMMC());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("Pixel Frequency", SystemInfo.getClockFrequencyPixel());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("VEC Frequency", SystemInfo.getClockFrequencyVEC());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("HDMI Frequency ", SystemInfo.getClockFrequencyHDMI());
			} catch (UnsupportedOperationException ex) {
			}
			try {
				root.put("DPI Frequency", SystemInfo.getClockFrequencyDPI());
			} catch (UnsupportedOperationException ex) {
			}
		} catch (NumberFormatException e) {
			root.put("ERROR", e.getLocalizedMessage());
		} catch (UnsupportedOperationException e) {
			root.put("ERROR", e.getLocalizedMessage());
		} catch (IOException e) {
			root.put("ERROR", e.getLocalizedMessage());
		} catch (InterruptedException e) {
			root.put("ERROR", e.getLocalizedMessage());
		} catch (ParseException e) {
			root.put("ERROR", e.getLocalizedMessage());
		}

		return root.toJSONString();
	}
}
