# TWRaspberryPiInfo

[![pipeline status](https://gitlab.com/IT-Berater/twraspberrypiinfo/badges/master/pipeline.svg)](https://gitlab.com/IT-Berater/twraspberrypiinfo/commits/master)

REST Service für die Systeminfo eines Raspberry Pi

Siehe auch www.wenzlaff.info